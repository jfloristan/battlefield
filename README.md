# Battlefield game for Tech Exchange 2019

## Background

## Requirements

* Openshift cluster version 4.1 with access to Red Hat Operators
* Two configured users:
  - Cluster admin user (i.e. admin)
  - Non-admin user to deploy the game (i.e. developer)
* Ansible to perform the installation
* Openshift cli client (oc)

## Installing

The installation consists of a few steps, some of them could be skipped if there is a pre-existing Service Mesh deployment:

* Creating an Ansible Vault [MANDATORY]: Configure the connections settings and variables needed
* Install Service Mesh [OPTIONAL]: If there is a pre-existing Service Mesh, ignore this step
* Deploy Control Plane [OPTIONAL]: If there is a pre-existing Service Mesh, ignore this step
* Deploy Data Plane: [MANDATORY] Create data plane and add project to the Service Mesh Member Roll
* Deploy Battlefield Game [MANDATORY]: Deploy the Battlefield game
* Create a game and enjoy! [ABSOLUTELY RECOMMENDED!]

### Creating an Ansible Vault

In the directory that contains your cloned copy of this git repo, create an Ansible vault called vault.yml as follows:

```console
$ ansible-vault create vault.yml
```

The vault requires the following variables. Adjust the values to suit your environment.

* ocp_api_host: Endpoint of the OCP API
* ocp_app_username: User for the battlefield game deployment
* ocp_app_token: API token fort the application user
* ocp_app_namespace: Namespace/project name to deploy the battlefield game
* ocp_admin_username: Cluster admin user to install the Service Mesh and the game operator
* ocp_admin_token: API token for the cluster admin user

```yaml
---
ocp_api_host: "https://api.cluster.domain:6443"
ocp_app_username: "changeme"
ocp_app_token: "changeme"
ocp_admin_username: "changeme"
ocp_admin_token: "changeme"
ocp_app_namespace: "battlefield"
```

To obtain the token for each user you can login in OCP and retieve the token (valid for 24 hours)

```console
$ oc login -u <username> <ocp_api_host>
$ oc whoami --show-token
```

### [OPTIONAL] Install Service Mesh

We need to install Service Mesh from the Red Hat provided operators. Run the following playbook:

```console
$ ansible-playbook -i inventory.yml --ask-vault-pass install-service-mesh.yml
```

Wait for Service Mesh Operator to be installed and ready. To do so check if the operators progressed 
to the istio-system namespace

```console
$ oc get csv -n istio-system
```

You should see something like:

```console
NAME                                         DISPLAY                          VERSION               REPLACES   PHASE
elasticsearch-operator.4.1.15-201909041605   Elasticsearch Operator           4.1.15-201909041605              Succeeded
jaeger-operator.v1.13.1                      Jaeger Operator                  1.13.1                           Succeeded
kiali-operator.v1.0.5                        Kiali Operator                   1.0.5                            Succeeded
servicemeshoperator.v1.0.0                   Red Hat OpenShift Service Mesh   1.0.0                            Succeeded
```

### [OPTIONAL] Deploy Control Plane

We need to create the control plane for Service Mesh under the istio-system namespace, to do so, please execute:

```console
$ ansible-playbook -i inventory.yml --ask-vault-pass deploy-sm-controlplane.yml
```

Wait for Control Plane to be installed and ready. To do so, you can check the pods in namespace istio-sytem.
You should get 10 Pods in "Running" state applying the following filter:

```console
$ oc get pods -n istio-system | grep -e istio- -e grafana- -e prometheus-
```

Wait to see 10 PODs running, something like:

```console
grafana-7c9b49897-mpc47                                  2/2     Running   0          4m49s
istio-citadel-7cdf78f8d7-7qszq                           1/1     Running   0          7m27s
istio-egressgateway-757d97cc65-2x6v5                     1/1     Running   0          5m46s
istio-galley-59fd5df664-mn946                            1/1     Running   0          7m
istio-ingressgateway-79cf488c9b-22zkl                    1/1     Running   0          5m46s
istio-pilot-d8d6cfc5b-4cnxp                              2/2     Running   0          6m30s
istio-policy-54cb7c4659-zrhrs                            2/2     Running   0          6m45s
istio-sidecar-injector-576b857596-l7hfk                  1/1     Running   0          5m8s
istio-telemetry-5775c7d5ff-2f9ch                         2/2     Running   0          6m44s
prometheus-77c95d7588-4d4vs                              2/2     Running   0          7m17s
```

### Deploy Data Plan

Once the control plane is ready, in this step we need to get the application namespace added to the Service Mesh Member Roll.

* If you are deploying the application in a pre-existing Service Mesh, use the MANUAL process:
```console
$ oc -n <control plane project> patch --type='json' smmr default -p '[{"op": "add", "path": "/spec/members", "value":["'"<ocp_app_namespace>"'"]}]'
```

* If is a new deployment with no pre-existing Service Mesh, use the CREATION OF A NEW DATA PLANE process
```console
$ ansible-playbook -i inventory.yml --ask-vault-pass deploy-sm-dataplane.yml
```

### Deploy Battlefield Game

```console
$ ansible-playbook -i inventory.yml --ask-vault-pass deploy-battlefield-game.yml
```

### Create a game and enjoy!

Access the route created for the battlefield-ui application under the configured application namespace, for example:

* http://battlefield-ui-battlefield.apps.cluster.domain

In parallel under the namespace istio-system, as routes, you can access Kiali and Jaguer dashboards to observe in real 
time the traffic and interactions between microservices:

* https://kiali-istio-system.apps.name.domain
* https://jaeger-istio-system.apps.name.domain
